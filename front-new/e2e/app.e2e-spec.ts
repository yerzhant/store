import { FrontNewPage } from './app.po';

describe('front-new App', function() {
  let page: FrontNewPage;

  beforeEach(() => {
    page = new FrontNewPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
