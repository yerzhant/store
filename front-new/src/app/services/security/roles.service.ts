import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { DataHttpService } from '../data-http.service';
import { Role } from '../../models/security/role.entity';

@Injectable()
export class RolesService extends DataHttpService<Role> {

  constructor(http: Http) {
    super(http, '/admin/security/roles');
  }

  create() {
    return new Role();
  }
}
