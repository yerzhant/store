import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

import { DataHttpService } from '../data-http.service';
import { User } from './../../models/security/user.entity';

@Injectable()
export class UsersService extends DataHttpService<User> {

  constructor(http: Http) {
    super(http, '/admin/security/users');
  }

  create() {
    return new User();
  }
}
