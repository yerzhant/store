import { Http } from '@angular/http';

import { BaseHttpService } from './base-http.service';
import { BaseEntity } from '../models/base.entity';

export abstract class DataHttpService<E extends BaseEntity> extends BaseHttpService {

    constructor(http: Http, private readonly api: string) {
        super(http);
    }

    abstract create(): E;

    findById(id: number) {
        return this.get(this.api + '/' + id);
    }

    list() {
        return this.get(this.api);
    }

    save(e: E) {
        return this.post(this.api, e);
    }

    remove(e: E) {
        return this.delete(this.api, e.id);
    }
}
