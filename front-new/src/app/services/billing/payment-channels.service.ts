import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

import { DataHttpService } from './../data-http.service';
import { PaymentChannel } from './../../models/billing/payment-channel.entity';

@Injectable()
export class PaymentChannelsService extends DataHttpService<PaymentChannel> {

  constructor(http: Http) {
    super(http, '/admin/billing/payment-channels');
  }

  create() {
    return new PaymentChannel();
  }
}
