import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { NotificationService } from './../util/notification.service';
import { DataHttpService } from '../data-http.service';
import { StoresService } from './stores.service';

import { Payment } from './../../models/billing/payment.entity';

@Injectable()
export class PaymentsService extends DataHttpService<Payment> {

    private static readonly api = '/admin/billing/payments';

    private id: number;

    constructor(
        private storeService: StoresService,
        private notificationService: NotificationService,
        http: Http) {
        super(http, PaymentsService.api);
    }

    create() {
        let payment = new Payment();
        this.storeService.findById(this.id).subscribe(
            store => payment.contract.store = store,
            e => this.notificationService.error(e.statusText)
        );
        return payment;
    }

    getByStoreId(id: number) {
        this.id = id;
        return this.get(PaymentsService.api + '/store/' + id);
    }
}
