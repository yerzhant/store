import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { NotificationService } from './../util/notification.service';
import { DataHttpService } from '../data-http.service';
import { StoresService } from './stores.service';

import { Accrual } from './../../models/billing/accrual.entity';

@Injectable()
export class AccrualsService extends DataHttpService<Accrual> {

    private static readonly api = '/admin/billing/accruals';

    private id: number;

    constructor(
        private storeService: StoresService,
        private notificationService: NotificationService,
        http: Http) {
        super(http, AccrualsService.api);
    }

    create() {
        let accrual = new Accrual();
        this.storeService.findById(this.id).subscribe(
            store => accrual.contract.store = store,
            e => this.notificationService.error(e.statusText)
        );
        return accrual;
    }

    getByStoreId(id: number) {
        this.id = id;
        return this.get(AccrualsService.api + '/store/' + id);
    }
}
