import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { DataHttpService } from '../data-http.service';
import { Store } from '../../models/billing/store.entity';

@Injectable()
export class StoresService extends DataHttpService<Store> {

  constructor(http: Http) {
    super(http, '/admin/billing/stores');
  }

  create() {
    return new Store();
  }
}
