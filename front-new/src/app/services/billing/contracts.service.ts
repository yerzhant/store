import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { NotificationService } from './../util/notification.service';
import { DataHttpService } from '../data-http.service';
import { StoresService } from './stores.service';

import { Contract } from './../../models/billing/contract.entity';

@Injectable()
export class ContractsService extends DataHttpService<Contract> {

    private static readonly api = '/admin/billing/contracts';

    private id: number;

    constructor(
        private storeService: StoresService,
        private notificationService: NotificationService,
        http: Http) {
        super(http, ContractsService.api);
    }

    create() {
        let contract = new Contract();
        this.storeService.findById(this.id).subscribe(
            store => contract.store = store,
            e => this.notificationService.error(e.statusText)
        );
        return contract;
    }

    getByStoreId(id: number) {
        this.id = id;
        return this.get(ContractsService.api + '/store/' + id);
    }
}
