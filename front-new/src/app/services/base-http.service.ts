import { Http, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

export abstract class BaseHttpService {

  private static readonly apiPrefix = '/api';
  private static readonly options = new RequestOptions({ headers: new Headers({ 'Content-Type': 'application/json' }) });

  constructor(private http: Http) { }

  protected get(api: string, apiPrefix = BaseHttpService.apiPrefix) {
    return this.http.get(apiPrefix + api)
      .map(r => r.json().data)
      .catch(this.handleError);
  }

  protected post(api: string, data: any, apiPrefix = BaseHttpService.apiPrefix) {
    return this.http.post(apiPrefix + api, data, BaseHttpService.options)
      .map(r => r.json().data)
      .catch(this.handleError);
  }

  protected delete(api: string, id: number, apiPrefix = BaseHttpService.apiPrefix) {
    return this.http.delete(apiPrefix + api + '/' + id, BaseHttpService.options)
      .map(r => r.json().data)
      .catch(this.handleError);
  }

  private handleError(error: any) {
    return Observable.throw(error);
  }
}
