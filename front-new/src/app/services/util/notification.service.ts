import { Subject } from 'rxjs/Subject';

import { Message } from 'primeng/primeng';

export class NotificationService {

  private subject = new Subject<Message>();

  stream = this.subject.asObservable();

  info(detail: string, summary = 'Уведомление') {
    this.notify('info', summary, detail);
  }

  warn(detail: string, summary = 'Предупреждение') {
    this.notify('warn', summary, detail);
  }

  error(detail: string, summary = 'Ошибка') {
    this.notify('error', summary, detail);
  }

  private notify(severity: string, summary: string, detail: string) {
    this.subject.next({ severity: severity, summary: summary, detail: detail });
  }

}
