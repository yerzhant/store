import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { BaseHttpService } from '../base-http.service';
import { User } from '../../models/security/user.entity';

@Injectable()
export class AuthService extends BaseHttpService {

  constructor(http: Http) {
    super(http);
  }

  login() {
    let user = new User();
    user.name = 'user';
    user.password = 'password';

    this.post('/authentication/login', user, '').subscribe(
      data => console.log(data),
      error => console.log(error)
    );
  }
}
