import { Component, ViewChild, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';

import { StoresService } from '../../services/billing/stores.service';
import { DataTableComponent } from '../shared/data-table.component';

@Component({
  selector: 'app-stores',
  templateUrl: './stores.component.html'
})
export class StoresComponent implements AfterViewInit {

  @ViewChild(DataTableComponent) private dataTable: DataTableComponent;

  onShowContracts() {
    let storeId = this.dataTable.selectedItem.id;
    this.router.navigate(['/contracts/store', storeId]);
  }

  onShowPayments() {
    let storeId = this.dataTable.selectedItem.id;
    this.router.navigate(['/payments/store', storeId]);
  }

  constructor(private service: StoresService, private router: Router) { }

  ngAfterViewInit() {
    this.dataTable.onRefresh();
  }
}
