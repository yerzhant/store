import { Component, ViewChild, AfterViewInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { PaymentsService } from './../../services/billing/payments.service';
import { NotificationService } from './../../services/util/notification.service';
import { DataTableComponent } from './../shared/data-table.component';

@Component({
  selector: 'app-payments',
  templateUrl: './payments.component.html'
})
export class PaymentsComponent implements AfterViewInit {

  @ViewChild(DataTableComponent) private dataTable: DataTableComponent;

  onRefresh() {
    this.ngAfterViewInit();
  }

  constructor(
    private service: PaymentsService,
    private route: ActivatedRoute,
    private notificationService: NotificationService
  ) { }

  ngAfterViewInit() {
    let storeId = this.route.snapshot.params['id'];

    this.service.getByStoreId(storeId).subscribe(
      payments => this.dataTable.items = payments,
      e => this.notificationService.error(e.statusText)
    );
  }
}
