import { Component, ViewChild, AfterViewInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ContractsService } from './../../services/billing/contracts.service';
import { NotificationService } from './../../services/util/notification.service';
import { DataTableComponent } from './../shared/data-table.component';

@Component({
  selector: 'app-contracts',
  templateUrl: './contracts.component.html'
})
export class ContractsComponent implements AfterViewInit {

  @ViewChild(DataTableComponent) private dataTable: DataTableComponent;

  onRefresh() {
    this.ngAfterViewInit();
  }

  constructor(
    private service: ContractsService,
    private route: ActivatedRoute,
    private notificationService: NotificationService
  ) { }

  ngAfterViewInit() {
    let storeId = this.route.snapshot.params['id'];

    this.service.getByStoreId(storeId).subscribe(
      contracts => this.dataTable.items = contracts,
      e => this.notificationService.error(e.statusText)
    );
  }
}
