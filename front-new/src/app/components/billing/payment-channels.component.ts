import { Component, ViewChild, AfterViewInit } from '@angular/core';

import { DataTableComponent } from './../shared/data-table.component';
import { PaymentChannelsService } from './../../services/billing/payment-channels.service';

@Component({
  selector: 'app-payment-channels',
  templateUrl: './payment-channels.component.html'
})
export class PaymentChannelsComponent implements AfterViewInit {

  @ViewChild(DataTableComponent) private dataTable: DataTableComponent;

  constructor(private service: PaymentChannelsService) { }

  ngAfterViewInit() {
    this.dataTable.onRefresh();
  }
}
