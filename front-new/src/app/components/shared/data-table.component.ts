import { Component, AfterViewInit, Input, Output, ContentChildren, QueryList, ViewChild, EventEmitter } from '@angular/core';
import { Column, ConfirmationService, DataTable } from 'primeng/primeng';

import { DataHttpService } from '../../services/data-http.service';
import { NotificationService } from '../../services/util/notification.service';
import { BaseEntity } from '../../models/base.entity';

@Component({
  selector: 'app-data-table',
  templateUrl: './data-table.component.html'
})
export class DataTableComponent implements AfterViewInit {

  selectedItem: BaseEntity;

  private editingItem: BaseEntity;

  items: BaseEntity[] = [];

  private isEditDialogVisible = false;

  @Input() private service: DataHttpService<BaseEntity>;

  @Input() protected editDialogWidth: number;

  @Output() private refresh = new EventEmitter();

  @Output() private save = new EventEmitter();

  @Output() private setArrayElements = new EventEmitter();

  @ViewChild(DataTable) private dataTable: DataTable;

  @ContentChildren(Column) protected columns: QueryList<Column>;

  onRefresh() {
    if (this.refresh.observers.length > 0) {
      this.refresh.emit();
    } else {
      this.service.list().subscribe(
        items => this.items = items,
        e => this.notificationService.error(e.statusText)
      );
    }
    this.selectedItem = null;
  }

  protected onAdd() {
    this.editingItem = this.service.create();
    this.isEditDialogVisible = true;
  }

  protected onEdit() {
    this.onAdd();
    this.clone(this.selectedItem, this.editingItem);
  }

  protected onSave() {
    this.save.emit(this.editingItem);

    this.service.save(this.editingItem).subscribe(
      id => {
        if (this.editingItem.id == null) {
          this.editingItem.id = id;
          this.items.push(this.editingItem);
        } else {
          let e = this.items.find(e => e.id === id);
          this.clone(this.editingItem, e, false);
        }
        this.isEditDialogVisible = false;
      },
      e => this.notificationService.error(e.status)
    );
  }

  protected onDelete() {
    this.confirmationService.confirm({
      message: 'Удалить запись?',
      accept: () =>
        this.service.remove(this.selectedItem).subscribe(
          () => {
            this.items.splice(this.items.indexOf(this.selectedItem), 1);
            this.selectedItem = null;
          },
          e => this.notificationService.error(e.statusText)
        )
    });
  }

  private clone(from: BaseEntity, to: BaseEntity, deep = true) {
    for (let prop in from) {
      if (from.hasOwnProperty(prop)) {
        if (deep && Array.isArray(from[prop])) {
          this.setArrayElements.emit({ prop: prop, from: from[prop], to: to[prop] });
        } else {
          to[prop] = from[prop];
        }
      }
    }
  }

  constructor(private notificationService: NotificationService, private confirmationService: ConfirmationService) { }

  ngAfterViewInit() {
    this.dataTable.cols = this.columns;
    this.dataTable.ngAfterContentInit();
  }

}
