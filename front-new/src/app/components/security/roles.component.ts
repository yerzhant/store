import { Component, ViewChild, AfterViewInit } from '@angular/core';

import { RolesService } from '../../services/security/roles.service';
import { DataTableComponent } from '../shared/data-table.component';

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html'
})
export class RolesComponent implements AfterViewInit {

  @ViewChild(DataTableComponent) private dataTable: DataTableComponent;

  constructor(private service: RolesService) { }

  ngAfterViewInit() {
    this.dataTable.onRefresh();
  }
}
