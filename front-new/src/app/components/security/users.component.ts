import { Component, ViewChild, OnInit, AfterViewInit } from '@angular/core';

import { NotificationService } from './../../services/util/notification.service';
import { StoresService } from './../../services/billing/stores.service';
import { RolesService } from './../../services/security/roles.service';
import { UsersService } from './../../services/security/users.service';

import { Store } from './../../models/billing/store.entity';
import { User } from './../../models/security/user.entity';
import { Role } from './../../models/security/role.entity';

import { DataTableComponent } from './../shared/data-table.component';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html'
})
export class UsersComponent implements OnInit, AfterViewInit {

  private roles: Role[];

  private stores: Store[];

  private isSetPasswordDialogVisible = false;

  @ViewChild(DataTableComponent) private dataTable;

  constructor(
    private service: UsersService,
    private rolesService: RolesService,
    private storesService: StoresService,
    private notificationService: NotificationService) { }

  ngOnInit() {
    this.rolesService.list().subscribe(
      roles => this.roles = roles
    );

    this.storesService.list().subscribe(
      stores => this.stores = stores
    );
  }

  protected onSetPassword() {
    this.service.save(this.dataTable.selectedItem).subscribe(
      () => this.notificationService.info('Password set.'),
      e => this.notificationService.error(e.errorText)
    );
    this.isSetPasswordDialogVisible = false;
  }

  protected onSave(user: User) {
    user.password = '-';
  }

  protected onSetArrayElements(event) {
    if (event.prop === 'appRoles') {
      event.from.forEach(e => event.to.push(this.roles.find(i => i.id === e.id)));
    } else {
      event.from.forEach(e => event.to.push(this.stores.find(i => i.id === e.id)));
    }
  }

  protected arrayToNameList(array: any[]) {
    return array.map(i => i.name).join(', ');
  }

  ngAfterViewInit() {
    this.dataTable.onRefresh();
  }
}
