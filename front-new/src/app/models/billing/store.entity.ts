import { BaseEntity } from '../base.entity';

export class Store extends BaseEntity {

    public name: string;
    public address: string;
}
