import { BaseEntity } from './../base.entity';

export class PaymentChannel extends BaseEntity {

    public name: string;
    public code: string;
}
