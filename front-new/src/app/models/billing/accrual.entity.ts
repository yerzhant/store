import { Contract } from './contract.entity';
import { BaseEntity } from '../base.entity';

export class Accrual extends BaseEntity {

    public accruedOn: Date;
    public amount: number;
    public contract: Contract;

}
