import { PaymentChannel } from './payment-channel.entity';
import { Contract } from './contract.entity';
import { BaseEntity } from '../base.entity';

export class Payment extends BaseEntity {

    public paidOn: Date;
    public amount: number;
    public contract: Contract;
    public paymentChannel: PaymentChannel;
    public description: string;

}
