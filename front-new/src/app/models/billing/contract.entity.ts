import { Store } from './store.entity';
import { BaseEntity } from '../base.entity';

export class Contract extends BaseEntity {

    public beginDate: Date;
    public endDate: Date;
    public initialFee: number;
    public monthlyFee: number;
    public store: Store;

}
