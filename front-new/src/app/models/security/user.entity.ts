import { BaseEntity } from '../base.entity';
import { Role } from './role.entity';
import { Store } from './../billing/store.entity';

export class User extends BaseEntity {

  public name: string;
  public password: string;
  public isLocked: boolean;
  public leftTries: number;
  // public contact:
  public stores: Store[] = [];
  public appRoles: Role[] = [];

}
