import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import {
  GrowlModule,
  MenubarModule,
  InputTextModule,
  ButtonModule,
  DataTableModule,
  ConfirmDialogModule,
  ConfirmationService,
  DialogModule,
  InputSwitchModule,
  CheckboxModule,
  PasswordModule
} from 'primeng/primeng';

import { routing } from './app.routing';
import { AuthService } from './services/auth/auth.service';
import { NotificationService } from './services/util/notification.service';
import { RolesService } from './services/security/roles.service';
import { UsersService } from './services/security/users.service';
import { StoresService } from './services/billing/stores.service';
import { PaymentChannelsService } from './services/billing/payment-channels.service';
import { ContractsService } from './services/billing/contracts.service';
import { AccrualsService } from './services/billing/accruals.service';
import { PaymentsService } from './services/billing/payments.service';

import { AppComponent } from './app.component';
import { RolesComponent } from './components/security/roles.component';
import { StoresComponent } from './components/billing/stores.component';
import { DataTableComponent } from './components/shared/data-table.component';
import { UsersComponent } from './components/security/users.component';
import { PaymentChannelsComponent } from './components/billing/payment-channels.component';
import { ContractsComponent } from './components/billing/contracts.component';
import { PaymentsComponent } from './components/billing/payments.component';

@NgModule({
  declarations: [
    AppComponent,
    RolesComponent,
    StoresComponent,
    DataTableComponent,
    UsersComponent,
    PaymentChannelsComponent,
    ContractsComponent,
    PaymentsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,

    GrowlModule,
    MenubarModule,
    InputTextModule,
    ButtonModule,
    DataTableModule,
    ConfirmDialogModule,
    DialogModule,
    InputSwitchModule,
    CheckboxModule,
    PasswordModule,

    routing
  ],
  providers: [
    ConfirmationService,

    AuthService,
    NotificationService,
    RolesService,
    UsersService,
    StoresService,
    PaymentChannelsService,
    ContractsService,
    PaymentsService,
    AccrualsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
