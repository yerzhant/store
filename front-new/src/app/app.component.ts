import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { Message, MenuItem } from 'primeng/primeng';

import { AuthService } from './services/auth/auth.service';
import { NotificationService } from './services/util/notification.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {

  private notifications: Message[] = [];
  private notificationsSubscription: Subscription;

  protected menu: MenuItem[] = [{
    label: 'Admin',
    items: [
      { label: 'Roles', routerLink: ['/roles'] },
      { label: 'Users', routerLink: ['/users'] },
      { label: 'Stores', routerLink: ['/stores'] },
      { label: 'Payment channels', routerLink: ['/payment-channels'] }
    ]
  }];

  constructor(private authService: AuthService, private notifcationService: NotificationService) {
  }

  onLogin() {
    this.authService.login();
  }

  ngOnInit() {
    this.onLogin();
    this.notificationsSubscription = this.notifcationService.stream.subscribe(
      message => this.notifications.push(message)
    );
  }

  ngOnDestroy() {
    this.notificationsSubscription.unsubscribe();
  }

}
