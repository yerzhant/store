import { Routes, RouterModule } from '@angular/router';

import { RolesComponent } from './components/security/roles.component';
import { UsersComponent } from './components/security/users.component';
import { StoresComponent } from './components/billing/stores.component';
import { PaymentChannelsComponent } from './components/billing/payment-channels.component';
import { ContractsComponent } from './components/billing/contracts.component';
import { PaymentsComponent } from './components/billing/payments.component';

const routes: Routes = [
  { path: 'roles', component: RolesComponent },
  { path: 'users', component: UsersComponent },
  { path: 'stores', component: StoresComponent },
  { path: 'payment-channels', component: PaymentChannelsComponent },
  { path: 'contracts/store/:id', component: ContractsComponent },
  { path: 'payments/store/:id', component: PaymentsComponent },
  { path: '', redirectTo: '/users', pathMatch: 'full' }
  // { path: '**', component: PageNotFoundComponent }
];

export const routing = RouterModule.forRoot(routes);
