package kz.store.config;

import kz.store.security.auth.AuthenticationFilter;
import kz.store.security.auth.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;

//@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private AuthenticationService authenticationService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        final AuthenticationFilter authenticationFilter = new AuthenticationFilter(authenticationService);
        authenticationFilter.setAuthenticationSuccessHandler((a, b, c) -> {
        });

        http
                .csrf().csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse()).and()
//                .csrf().disable() // For testings
                .logout().disable()
                .addFilterBefore(authenticationFilter, BasicAuthenticationFilter.class)
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                .authorizeRequests()
                .antMatchers("/api/**").authenticated()
                .antMatchers("/api/store/admin/**").hasRole("store-admin")
                .antMatchers("/api/admin/**").hasRole("admin");
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring()
                //                .antMatchers("/static/js/**")
                //                .antMatchers("/static/app/**/*.{html}")
                .antMatchers("/authentication/login");
    }

    @Autowired
    public void configureAuthenticationMananger(AuthenticationManagerBuilder auth) {
        auth.authenticationProvider(null);
    }
}
