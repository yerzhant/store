package kz.store.repository;

import kz.store.entity.ProductFlowItem;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductFlowItemRepository extends JpaRepository<ProductFlowItem, Long> {

}
