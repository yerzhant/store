package kz.store.repository.billing;

import kz.store.entity.billing.PaymentChannel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PaymentChannelRepository extends JpaRepository<PaymentChannel, Long> {

}
