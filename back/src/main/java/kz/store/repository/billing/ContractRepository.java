package kz.store.repository.billing;

import java.util.List;
import kz.store.entity.billing.Contract;
import kz.store.entity.billing.Store;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ContractRepository extends JpaRepository<Contract, Long> {

    List<Contract> findByStore(Store store);
}
