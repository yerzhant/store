package kz.store.repository.billing;

import java.util.List;
import kz.store.entity.billing.Accrual;
import kz.store.entity.billing.Store;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccrualRepository extends JpaRepository<Accrual, Long> {

    List<Accrual> findByContractStore(Store store);
}
