package kz.store.repository.billing;

import java.util.List;
import kz.store.entity.billing.Payment;
import kz.store.entity.billing.Store;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PaymentRepository extends JpaRepository<Payment, Long> {

    List<Payment> findByContractStore(Store store);
}
