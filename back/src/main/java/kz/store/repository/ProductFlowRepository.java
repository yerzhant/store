package kz.store.repository;

import kz.store.entity.ProductFlow;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductFlowRepository extends JpaRepository<ProductFlow, Long> {

}
