package kz.store.service;

import java.util.List;
import kz.store.entity.BaseEntity;

public interface BaseService<T extends BaseEntity> {

    List<T> findAll();

    T findOne(Long id);

    T save(T entity);

    void delete(Long id);
}
