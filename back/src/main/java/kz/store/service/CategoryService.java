package kz.store.service;

import kz.store.entity.Category;

public interface CategoryService extends BaseService<Category> {

}
