package kz.store.service;

import kz.store.entity.Manufacturer;

public interface ManufacturerService extends BaseService<Manufacturer> {

}
