package kz.store.service;

import kz.store.entity.Product;

public interface ProductService extends BaseService<Product> {

}
