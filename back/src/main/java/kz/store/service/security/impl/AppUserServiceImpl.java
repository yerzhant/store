package kz.store.service.security.impl;

import kz.store.entity.security.AppUser;
import kz.store.repository.security.AppUserRepository;
import kz.store.service.impl.BaseServiceImpl;
import kz.store.service.impl.ServiceRepository;
import kz.store.service.security.AppUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AppUserServiceImpl extends BaseServiceImpl<AppUser> implements AppUserService {

    @Autowired
    @ServiceRepository
    private AppUserRepository repository;

    @Override
    public AppUser findByName(String name) {
        return repository.findByName(name);
    }
}
