package kz.store.service.security;

import kz.store.entity.security.AppUser;
import kz.store.service.BaseService;

public interface AppUserService extends BaseService<AppUser> {

    AppUser findByName(String name);
}
