package kz.store.service.security;

import kz.store.entity.security.AppRole;
import kz.store.service.BaseService;

public interface AppRoleService extends BaseService<AppRole> {

}
