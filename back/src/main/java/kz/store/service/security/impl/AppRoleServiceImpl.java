package kz.store.service.security.impl;

import kz.store.entity.security.AppRole;
import kz.store.repository.security.AppRoleRepository;
import kz.store.service.impl.BaseServiceImpl;
import kz.store.service.impl.ServiceRepository;
import kz.store.service.security.AppRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AppRoleServiceImpl extends BaseServiceImpl<AppRole> implements AppRoleService {

    @Autowired
    @ServiceRepository
    private AppRoleRepository repository;
}
