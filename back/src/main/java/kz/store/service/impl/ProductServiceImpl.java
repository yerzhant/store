package kz.store.service.impl;

import kz.store.entity.Product;
import kz.store.repository.ProductRepository;
import kz.store.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductServiceImpl extends BaseServiceImpl<Product> implements ProductService {

    @Autowired
    @ServiceRepository
    private ProductRepository repository;
}
