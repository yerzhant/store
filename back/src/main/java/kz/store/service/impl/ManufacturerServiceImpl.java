package kz.store.service.impl;

import kz.store.entity.Manufacturer;
import kz.store.repository.ManufacturerRepository;
import kz.store.service.ManufacturerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ManufacturerServiceImpl extends BaseServiceImpl<Manufacturer> implements ManufacturerService {

    @Autowired
    @ServiceRepository
    private ManufacturerRepository repository;
}
