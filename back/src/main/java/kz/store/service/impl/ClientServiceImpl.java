package kz.store.service.impl;

import kz.store.entity.Client;
import kz.store.repository.ClientRepository;
import kz.store.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClientServiceImpl extends BaseServiceImpl<Client> implements ClientService {

    @Autowired
    @ServiceRepository
    private ClientRepository repository;
}
