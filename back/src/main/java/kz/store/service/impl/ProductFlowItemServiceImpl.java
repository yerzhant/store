package kz.store.service.impl;

import kz.store.entity.ProductFlowItem;
import kz.store.repository.ProductFlowItemRepository;
import kz.store.service.ProductFlowItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductFlowItemServiceImpl extends BaseServiceImpl<ProductFlowItem> implements ProductFlowItemService {

    @Autowired
    @ServiceRepository
    private ProductFlowItemRepository repository;
}
