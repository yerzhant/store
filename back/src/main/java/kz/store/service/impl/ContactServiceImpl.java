package kz.store.service.impl;

import kz.store.entity.Contact;
import kz.store.repository.ContactRepository;
import kz.store.service.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ContactServiceImpl extends BaseServiceImpl<Contact> implements ContactService {

    @Autowired
    @ServiceRepository
    private ContactRepository repository;
}
