package kz.store.service.impl;

import kz.store.entity.ProductFlow;
import kz.store.repository.ProductFlowRepository;
import kz.store.service.ProductFlowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductFlowServiceImpl extends BaseServiceImpl<ProductFlow> implements ProductFlowService {

    @Autowired
    @ServiceRepository
    private ProductFlowRepository repository;
}
