package kz.store.service.impl;

import kz.store.entity.Category;
import kz.store.repository.CategoryRepository;
import kz.store.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CategoryServiceImpl extends BaseServiceImpl<Category> implements CategoryService {

    @Autowired
    @ServiceRepository
    private CategoryRepository repository;
}
