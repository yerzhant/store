package kz.store.service.impl;

import kz.store.entity.Image;
import kz.store.repository.ImageRepository;
import kz.store.service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ImageServiceImpl extends BaseServiceImpl<Image> implements ImageService {

    @Autowired
    @ServiceRepository
    private ImageRepository repository;
}
