package kz.store.service.impl;

import kz.store.entity.Supplier;
import kz.store.repository.SupplierRepository;
import kz.store.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SupplierServiceImpl extends BaseServiceImpl<Supplier> implements SupplierService {

    @Autowired
    @ServiceRepository
    private SupplierRepository repository;
}
