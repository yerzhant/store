package kz.store.service.billing.impl;

import java.util.List;
import kz.store.entity.billing.Contract;
import kz.store.entity.billing.Store;
import kz.store.repository.billing.ContractRepository;
import kz.store.service.billing.ContractService;
import kz.store.service.impl.BaseServiceImpl;
import kz.store.service.impl.ServiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ContractServiceImpl extends BaseServiceImpl<Contract> implements ContractService {

    @Autowired
    @ServiceRepository
    private ContractRepository repository;

    @Override
    public List<Contract> findByStore(Store store) {
        return repository.findByStore(store);
    }
}
