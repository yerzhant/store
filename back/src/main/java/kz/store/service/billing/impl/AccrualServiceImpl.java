package kz.store.service.billing.impl;

import java.util.List;
import kz.store.entity.billing.Accrual;
import kz.store.entity.billing.Store;
import kz.store.repository.billing.AccrualRepository;
import kz.store.service.billing.AccrualService;
import kz.store.service.impl.BaseServiceImpl;
import kz.store.service.impl.ServiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccrualServiceImpl extends BaseServiceImpl<Accrual> implements AccrualService {

    @Autowired
    @ServiceRepository
    private AccrualRepository repository;

    @Override
    public List<Accrual> findByStore(Store store) {
        return repository.findByContractStore(store);
    }
}
