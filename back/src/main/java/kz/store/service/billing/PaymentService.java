package kz.store.service.billing;

import java.util.List;
import kz.store.entity.billing.Payment;
import kz.store.entity.billing.Store;
import kz.store.service.BaseService;

public interface PaymentService extends BaseService<Payment> {

    List<Payment> findByStore(Store store);
}
