package kz.store.service.billing;

import kz.store.entity.billing.PaymentChannel;
import kz.store.service.BaseService;

public interface PaymentChannelService extends BaseService<PaymentChannel> {

}
