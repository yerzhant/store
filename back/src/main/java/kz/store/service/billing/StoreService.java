package kz.store.service.billing;

import kz.store.entity.billing.Store;
import kz.store.service.BaseService;

public interface StoreService extends BaseService<Store> {

}
