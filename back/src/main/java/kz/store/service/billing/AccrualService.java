package kz.store.service.billing;

import java.util.List;
import kz.store.entity.billing.Accrual;
import kz.store.entity.billing.Store;
import kz.store.service.BaseService;

public interface AccrualService extends BaseService<Accrual> {
    
    List<Accrual> findByStore(Store store);
}
