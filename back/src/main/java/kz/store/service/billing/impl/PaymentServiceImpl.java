package kz.store.service.billing.impl;

import java.util.List;
import kz.store.entity.billing.Payment;
import kz.store.entity.billing.Store;
import kz.store.repository.billing.PaymentRepository;
import kz.store.service.billing.PaymentService;
import kz.store.service.impl.BaseServiceImpl;
import kz.store.service.impl.ServiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PaymentServiceImpl extends BaseServiceImpl<Payment> implements PaymentService {

    @Autowired
    @ServiceRepository
    private PaymentRepository repository;

    @Override
    public List<Payment> findByStore(Store store) {
        return repository.findByContractStore(store);
    }
}
