package kz.store.service.billing.impl;

import kz.store.entity.billing.Store;
import kz.store.repository.billing.StoreRepository;
import kz.store.service.billing.StoreService;
import kz.store.service.impl.BaseServiceImpl;
import kz.store.service.impl.ServiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StoreServiceImpl extends BaseServiceImpl<Store> implements StoreService {

    @Autowired
    @ServiceRepository
    private StoreRepository repository;
}
