package kz.store.service.billing.impl;

import kz.store.entity.billing.PaymentChannel;
import kz.store.repository.billing.PaymentChannelRepository;
import kz.store.service.billing.PaymentChannelService;
import kz.store.service.impl.BaseServiceImpl;
import kz.store.service.impl.ServiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PaymentChannelServiceImpl extends BaseServiceImpl<PaymentChannel> implements PaymentChannelService {

    @Autowired
    @ServiceRepository
    private PaymentChannelRepository repository;
}
