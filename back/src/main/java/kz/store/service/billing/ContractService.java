package kz.store.service.billing;

import java.util.List;
import kz.store.entity.billing.Contract;
import kz.store.entity.billing.Store;
import kz.store.service.BaseService;

public interface ContractService extends BaseService<Contract> {

    List<Contract> findByStore(Store store);
}
