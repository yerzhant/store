package kz.store.service;

import kz.store.entity.Supplier;

public interface SupplierService extends BaseService<Supplier> {

}
