package kz.store.entity;

import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import kz.store.entity.billing.Store;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Data
public class Supplier extends BaseEntity {

    @NotEmpty
    private String name;

    private String address;

    @ManyToMany
    private Set<Contact> contacts;

    @NotNull
    @ManyToOne
    private Store store;
}
