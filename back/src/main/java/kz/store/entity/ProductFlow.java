package kz.store.entity;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import kz.store.entity.billing.Store;
import kz.store.enumeration.ProductFlowStatus;
import lombok.Data;

@Entity
@Data
public class ProductFlow extends BaseEntity {

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date flowedOn = new Date();

    @NotNull
    private Boolean isInward;

    @NotNull
    private ProductFlowStatus status = ProductFlowStatus.NEW;

    private String number;

    private String description;

    @ManyToOne
    private Supplier supplier;

    @ManyToOne
    private Client client;

    @NotNull
    @ManyToOne
    private Store store;
}
