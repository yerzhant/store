package kz.store.entity.billing;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import kz.store.entity.BaseEntity;
import lombok.Data;

@Entity
@Data
public class Payment extends BaseEntity {

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date paidOn = new Date();

    @NotNull
    private BigDecimal amount;

    @NotNull
    @ManyToOne
    private Contract contract;

    @NotNull
    @ManyToOne
    private PaymentChannel paymentChannel;

    private String description;
}
