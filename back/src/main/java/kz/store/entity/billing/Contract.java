package kz.store.entity.billing;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import kz.store.entity.BaseEntity;
import lombok.Data;

@Entity
@Data
public class Contract extends BaseEntity {

    @NotNull
    @Temporal(TemporalType.DATE)
    private Date beginDate;

    @Temporal(TemporalType.DATE)
    private Date endDate;

    @NotNull
    @DecimalMin("0")
    private BigDecimal initialFee;

    @NotNull
    @DecimalMin("0")
    private BigDecimal monthlyFee;

    @NotNull
    @ManyToOne
    private Store store;
}
