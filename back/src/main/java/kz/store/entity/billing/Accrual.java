package kz.store.entity.billing;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import kz.store.entity.BaseEntity;
import lombok.Data;

@Entity
@Data
public class Accrual extends BaseEntity {

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date accruedOn = new Date();

    @NotNull
    private BigDecimal amount;

    @NotNull
    @ManyToOne
    private Contract contract;
}
