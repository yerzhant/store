package kz.store.entity;

import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import kz.store.entity.billing.Store;
import kz.store.enumeration.ClientType;
import lombok.Data;

@Entity
@Data
public class Client extends BaseEntity {

    private ClientType clientType;

    private String name;

    private String lastName;

    private String firstName;

    private String surname;

    private String idNumber;

    @ManyToMany
    private Set<Contact> contacts;

    @NotNull
    @ManyToOne
    private Store store;
}
