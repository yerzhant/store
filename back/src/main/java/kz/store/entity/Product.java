package kz.store.entity;

import java.math.BigDecimal;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Data
public class Product extends BaseEntity {

    @NotEmpty
    private String name;

    @DecimalMin("0")
    private BigDecimal price;

    private String partNumber;

    private String remarks;

    @NotNull
    @ManyToOne
    private Category category;

    @ManyToOne
    private Manufacturer manufacturer;
}
