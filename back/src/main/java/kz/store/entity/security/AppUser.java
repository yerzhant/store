package kz.store.entity.security;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import kz.store.entity.BaseEntity;
import kz.store.entity.Contact;
import kz.store.entity.billing.Store;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.NaturalId;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Value;

@Entity
@Data
public class AppUser extends BaseEntity {

    @NotEmpty
    @NaturalId
    private String name;

    @Getter(onMethod = @__(
            @JsonIgnore))
    @Setter(onMethod = @__(
            @JsonProperty))
    @NotEmpty
    private String password;

    @NotNull
    private Boolean isLocked = false;

    @NotNull
    @Min(0)
    @Value("${store.security.loginTries}")
    private Integer leftTries;

    @ManyToMany
    private Set<Store> stores;

    @NotNull
    @ManyToMany
    private Set<AppRole> appRoles;

    @ManyToOne
    private Contact contact;

    public void decreaseLeftTries() {
        leftTries--;
    }
}
