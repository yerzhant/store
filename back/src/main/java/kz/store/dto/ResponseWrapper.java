package kz.store.dto;

import lombok.Data;

@Data
public class ResponseWrapper {

    private final Object data;
}
