package kz.store.dto;

import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

@Data
public class User {

    @NotEmpty
    private final String name;

    @NotEmpty
    private final String password;
}
