package kz.store.security.auth;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import io.jsonwebtoken.impl.crypto.MacProvider;
import java.security.Key;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import kz.store.dto.User;
import kz.store.entity.security.AppRole;
import kz.store.entity.security.AppUser;
import kz.store.service.security.AppUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AuthenticationService {

    private final static int EXPIRATION = 3; // in months

    private final static String ISSUER = UUID.randomUUID().toString();

    private final static Key KEY = MacProvider.generateKey();

    @Value("${store.security.loginTries}")
    private Integer loginTries;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    private AppUserService appUserService;

    @Transactional(noRollbackFor = BadCredentialsException.class)
    public UserInfo login(User user) {

        AppUser appUser = appUserService.findByName(user.getName());

        if (appUser == null || appUser.getLeftTries() == 0 || appUser.getIsLocked()) {
            throw new UsernameNotFoundException(user.getName());
        }

        if (!passwordEncoder.matches(user.getPassword(), appUser.getPassword())) {
            appUser.decreaseLeftTries();
            appUserService.save(appUser);
            throw new BadCredentialsException(user.getName());
        }

        appUser.setLeftTries(loginTries);
        appUserService.save(appUser);

        Calendar expiration = Calendar.getInstance();
        expiration.add(Calendar.MONTH, EXPIRATION);

        final Set<AppRole> appRoles = appUser.getAppRoles();

        final String jwt = Jwts.builder()
                .setIssuer(ISSUER)
                .setIssuedAt(new Date())
                .setSubject(user.getName())
                .setExpiration(expiration.getTime())
                .claim("roles", appRoles.stream().map(r -> "ROLE_" + r.getCode()).collect(Collectors.joining(",")))
                .signWith(SignatureAlgorithm.HS512, KEY)
                .compact();

        return new UserInfo(jwt, appRoles);
    }

    public Authentication authenticate(String jwt) {
        Claims body = null;

        try {
            body = Jwts.parser()
                    .requireIssuer(ISSUER)
                    .setSigningKey(KEY)
                    .parseClaimsJws(jwt)
                    .getBody();
        } catch (ExpiredJwtException | UnsupportedJwtException | MalformedJwtException | SignatureException | IllegalArgumentException ex) {
            throw new JwtAuthenticationExeception(ex.getMessage());
        }

        return new PreAuthenticatedAuthenticationToken(body.getSubject(), jwt, AuthorityUtils.commaSeparatedStringToAuthorityList(body.get("roles",
                String.class)));
    }

    public static class UserInfo {

        public String jwt;

        public Set<AppRole> roles;

        public UserInfo(String jwt, Set<AppRole> roles) {
            this.jwt = jwt;
            this.roles = roles;
        }
    }
}
