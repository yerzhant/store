package kz.store.security.auth;

import java.io.IOException;
import java.util.stream.Stream;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import kz.store.controller.authentication.AuthenticationController;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;

public class AuthenticationFilter extends AbstractAuthenticationProcessingFilter {

    private final AuthenticationService authenticationService;

    public AuthenticationFilter(AuthenticationService authenticationService) {
        super("/api/**");
        this.authenticationService = authenticationService;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException, IOException,
            ServletException {

        final Cookie[] cookies = request.getCookies();

        String jwt = cookies != null ? Stream.of(cookies).filter(c -> c.getName().equals(AuthenticationController.X_JWT_COOKIE_NAME)).
                map(Cookie::getValue).findAny().orElse("") : "";

        if (jwt.equals("")) {
            throw new AuthenticationCredentialsNotFoundException("Jwt cookie not found.");
        }

        return authenticationService.authenticate(jwt);
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws
            IOException, ServletException {
        super.successfulAuthentication(request, response, chain, authResult);

        chain.doFilter(request, response);
    }
}
