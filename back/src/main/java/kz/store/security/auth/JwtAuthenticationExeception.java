package kz.store.security.auth;

import org.springframework.security.core.AuthenticationException;

public class JwtAuthenticationExeception extends AuthenticationException {

    public JwtAuthenticationExeception(String message) {
        super(message);
    }
}
