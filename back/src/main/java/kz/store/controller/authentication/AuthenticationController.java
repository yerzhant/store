package kz.store.controller.authentication;

import java.util.Set;
import java.util.stream.Collectors;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import kz.store.dto.ResponseWrapper;
import kz.store.dto.User;
import kz.store.security.auth.AuthenticationService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/authentication")
public class AuthenticationController {

    public static final String X_JWT_COOKIE_NAME = "X-JWT";

    @Autowired
    private AuthenticationService authenticationService;

    @PostMapping("/login")
    public ResponseWrapper login(@Valid @RequestBody User user, HttpServletRequest request, HttpServletResponse response) {
        AuthenticationService.UserInfo userInfo = authenticationService.login(user);
        Cookie c = new Cookie(X_JWT_COOKIE_NAME, userInfo.jwt);
        c.setHttpOnly(true);
        c.setPath("/api");
        c.setMaxAge(-1);
        c.setSecure(request.isSecure());
        response.addCookie(c);
        return new ResponseWrapper(new Roles(userInfo.roles.stream().map(r -> r.getCode()).collect(Collectors.toSet())));
    }

    @Data
    public static class Roles {

        private final Set<String> roles;
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public String authenticationError(AuthenticationException ex) {
        return ex.getMessage();
    }
}
