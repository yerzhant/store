package kz.store.controller;

import kz.store.entity.Manufacturer;
import kz.store.service.ManufacturerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/store/admin/manufacturers")
@Secured("ROLE_store-admin")
public class ManufacturerController extends CrudController<Manufacturer> {

    @Autowired
    @ControllerService
    private ManufacturerService service;
}
