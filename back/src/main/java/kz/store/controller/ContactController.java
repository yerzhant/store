package kz.store.controller;

import kz.store.entity.Contact;
import kz.store.service.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/store/admin/contacts")
@Secured("ROLE_store-admin")
public class ContactController extends CrudController<Contact> {

    @Autowired
    @ControllerService
    private ContactService service;
}
