package kz.store.controller;

import kz.store.entity.ProductFlowItem;
import kz.store.service.ProductFlowItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/product-flow-items")
public class ProductFlowItemController extends CrudController<ProductFlowItem> {

    @Autowired
    @ControllerService
    private ProductFlowItemService service;
}
