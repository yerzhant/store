package kz.store.controller;

import kz.store.entity.ProductFlow;
import kz.store.service.ProductFlowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/product-flows")
public class ProductFlowController extends CrudController<ProductFlow> {

    @Autowired
    @ControllerService
    private ProductFlowService service;
}
