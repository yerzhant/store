package kz.store.controller;

import kz.store.entity.Image;
import kz.store.service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/images")
public class ImageController extends CrudController<Image> {

    @Autowired
    @ControllerService
    private ImageService service;
}
