package kz.store.controller.security;

import kz.store.controller.FullCrudController;
import kz.store.controller.ControllerService;
import kz.store.entity.security.AppRole;
import kz.store.service.security.AppRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/admin/security/roles")
@Secured("ROLE_admin")
public class AppRoleController extends FullCrudController<AppRole> {

    @Autowired
    @ControllerService
    private AppRoleService appRoleService;
}
