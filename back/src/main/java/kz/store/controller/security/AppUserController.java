package kz.store.controller.security;

import javax.validation.Valid;
import kz.store.controller.FullCrudController;
import kz.store.controller.ControllerService;
import kz.store.entity.security.AppUser;
import kz.store.service.security.AppUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/admin/security/users")
@Secured("ROLE_admin")
public class AppUserController extends FullCrudController<AppUser> {

    @Value("${store.security.loginTries}")
    private Integer loginTries;

    @Autowired
    @ControllerService
    private AppUserService appUserService;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Override
    @PostMapping
    public Long save(@Valid @RequestBody AppUser user) {
        if (user.getId() == null) {
            user.setLeftTries(loginTries);
            user.setPassword(passwordEncoder.encode(user.getPassword()));
        }
        return super.save(user);
    }
}
