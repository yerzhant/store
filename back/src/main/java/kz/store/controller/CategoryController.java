package kz.store.controller;

import kz.store.entity.Category;
import kz.store.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/store/admin/categories")
@Secured("ROLE_store-admin")
public class CategoryController extends CrudController<Category> {

    @Autowired
    @ControllerService
    private CategoryService service;
}
