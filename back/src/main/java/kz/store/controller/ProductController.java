package kz.store.controller;

import kz.store.entity.Product;
import kz.store.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/store/admin/products")
@Secured("ROLE_store-admin")
public class ProductController extends CrudController<Product> {

    @Autowired
    @ControllerService
    private ProductService service;
}
