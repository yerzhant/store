package kz.store.controller;

import java.lang.reflect.Field;
import javax.annotation.PostConstruct;
import javax.validation.Valid;
import kz.store.entity.BaseEntity;
import kz.store.service.BaseService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

public abstract class CrudController<T extends BaseEntity> {

    protected BaseService<T> service;

    @PostConstruct
    public void init() throws IllegalArgumentException, IllegalAccessException {
        for (Field f : this.getClass().getDeclaredFields()) {
            if (f.isAnnotationPresent(ControllerService.class)) {
                f.setAccessible(true);
                service = (BaseService<T>) f.get(this);
                break;
            }
        }
    }

    @GetMapping("/{id}")
    public T get(@PathVariable Long id) {
        return service.findOne(id);
    }

    @PostMapping
    public Long save(@Valid @RequestBody T entity) {
        return service.save(entity).getId();
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        service.delete(id);
    }
}
