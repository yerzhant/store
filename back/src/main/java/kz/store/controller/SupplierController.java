package kz.store.controller;

import kz.store.entity.Supplier;
import kz.store.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/store/admin/suppliers")
@Secured("ROLE_store-admin")
public class SupplierController extends CrudController<Supplier> {

    @Autowired
    @ControllerService
    private SupplierService service;
}
