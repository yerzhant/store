package kz.store.controller;

import java.util.List;
import kz.store.entity.BaseEntity;
import org.springframework.web.bind.annotation.GetMapping;

public abstract class FullCrudController<T extends BaseEntity> extends CrudController<T> {

    @GetMapping
    public List<T> findAll() {
        return service.findAll();
    }
}
