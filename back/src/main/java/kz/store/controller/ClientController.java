package kz.store.controller;

import kz.store.entity.Client;
import kz.store.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/store/admin/clients")
@Secured("ROLE_store-admin")
public class ClientController extends CrudController<Client> {

    @Autowired
    @ControllerService
    private ClientService service;
}
