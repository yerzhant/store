package kz.store.controller.billing;

import java.util.List;
import kz.store.controller.CrudController;
import kz.store.controller.ControllerService;
import kz.store.entity.billing.Accrual;
import kz.store.entity.billing.Store;
import kz.store.service.billing.AccrualService;
import kz.store.service.billing.StoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/admin/billing/accruals")
@Secured("ROLE_admin")
public class AccrualController extends CrudController<Accrual> {

    @Autowired
    @ControllerService
    private AccrualService accrualService;

    @Autowired
    private StoreService storeService;

    @GetMapping("/store/{storeId}")
    public List<Accrual> find(@PathVariable Long storeId) {
        Store store = storeService.findOne(storeId);
        return accrualService.findByStore(store);
    }
}
