package kz.store.controller.billing;

import kz.store.controller.FullCrudController;
import kz.store.controller.ControllerService;
import kz.store.entity.billing.PaymentChannel;
import kz.store.service.billing.PaymentChannelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/admin/billing/payment-channels")
@Secured("ROLE_admin")
public class PaymentChannelController extends FullCrudController<PaymentChannel> {

    @Autowired
    @ControllerService
    private PaymentChannelService paymentChannelService;
}
