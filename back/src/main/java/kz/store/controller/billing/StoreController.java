package kz.store.controller.billing;

import kz.store.controller.FullCrudController;
import kz.store.controller.ControllerService;
import kz.store.entity.billing.Store;
import kz.store.service.billing.StoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/admin/billing/stores")
@Secured("ROLE_admin")
public class StoreController extends FullCrudController<Store> {

    @Autowired
    @ControllerService
    private StoreService storeService;
}
