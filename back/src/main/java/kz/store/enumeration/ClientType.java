package kz.store.enumeration;

public enum ClientType {

    INDIVIDUAL, ENTREPRENEUR, LEGAL_ENTITY
}
