package kz.store.entity.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class AppRolesTests {

    @Autowired
    private WebApplicationContext context;

    private MockMvc mvc;

    @Before
    public void setUp() {
        mvc = MockMvcBuilders.webAppContextSetup(context).build();
    }

    @Test
    public void testSizeAndName() throws Exception {
        mvc.perform(get("/api/admin/security/roles"))
                .andExpect(status().isOk())
//                .andDo(MockMvcResultHandlers.print())
                .andExpect(jsonPath("$.data.length()", greaterThan(0)))
                .andExpect(jsonPath("$.data[0].name", equalTo("Admin")));
    }

    @Test
    public void testCreate() throws Exception {
        AppRole ar = new AppRole();
        ar.setName("Test");
        ar.setCode("TEST");

        mvc.perform(post("/api/admin/security/roles").contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsBytes(ar)))
                .andExpect(status().isOk());
    }
}
