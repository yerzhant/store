package kz.store.repository.security;

import kz.store.entity.security.AppRole;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class AppRoleRepositoryTest {

    @Autowired
    private AppRoleRepository r;

    private AppRole ar;

    @Before
    public void setUp() {
        ar = new AppRole();
        ar.setName("Test");
        ar.setCode("TEST");
        r.save(ar);
    }

    @Test
    public void testCreate() {
        assertNotNull(ar.getId());
    }

}
