package kz.store.repository;

import java.util.List;
import kz.store.entity.Manufacturer;
import kz.store.entity.billing.Store;
import kz.store.repository.billing.StoreRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class ManufacturerRepositoryTest {

    @Autowired
    private ManufacturerRepository r;

    @Autowired
    private StoreRepository sr;

    Manufacturer m;

    Store s;

    @Before
    public void setUp() {
        s = sr.findAll().get(0);

        m = new Manufacturer();
        m.setName("Test");
        m.setStore(s);
        r.save(m);
    }

    @Test
    public void testFindAll() {
        List<Manufacturer> l = r.findAll();
        assertTrue(l.size() > 0);
    }

    @Test
    public void testFindOne() {
        Manufacturer m2 = r.findOne(m.getId());
        assertEquals(m, m2);
    }

    @Test
    public void testCreate() {
        assertNotNull(m.getId());
    }

    @Test
    public void testUpdate() {
        Manufacturer m2 = r.findOne(m.getId());
        m2.setName("New test");
        r.save(m2);
        Manufacturer m3 = r.findOne(m2.getId());
        assertEquals(m3, m2);
    }

    @Test(expected = javax.validation.ConstraintViolationException.class)
    public void testSetNullName() {
        Manufacturer m2 = new Manufacturer();
        m2.setName(null);
        m2.setStore(s);
        r.save(m2);
    }

    @Test(expected = javax.validation.ConstraintViolationException.class)
    public void testSetEmptyName() {
        Manufacturer m2 = new Manufacturer();
        m2.setName("");
        m2.setStore(s);
        r.save(m2);
    }

    @Test
    public void testDelete() {
        r.delete(m);
        assertNull(r.findOne(m.getId()));
    }
}
