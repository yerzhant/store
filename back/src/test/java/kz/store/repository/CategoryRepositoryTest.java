package kz.store.repository;

import java.util.List;
import kz.store.entity.Category;
import kz.store.entity.billing.Store;
import kz.store.repository.billing.StoreRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class CategoryRepositoryTest {

    @Autowired
    private CategoryRepository r;

    @Autowired
    private StoreRepository sr;

    Category c;

    Store s;

    @Before
    public void setUp() {
        s = sr.findAll().get(0);

        c = new Category();
        c.setName("Test");
        c.setStore(s);
        r.save(c);
    }

    @Test
    public void testFindAll() {
        List<Category> l = r.findAll();
        assertTrue(l.size() > 0);
    }

    @Test
    public void testFindOne() {
        Category c2 = r.findOne(c.getId());
        assertEquals(c, c2);
    }

    @Test
    public void testCreate() {
        assertNotNull(c.getId());
    }

    @Test
    public void testUpdate() {
        Category c2 = r.findOne(c.getId());
        c2.setName("New test");
        r.save(c2);
        Category c3 = r.findOne(c2.getId());
        assertEquals(c2, c3);
    }

    @Test(expected = javax.validation.ConstraintViolationException.class)
    public void testSetNullName() {
        Category c2 = new Category();
        c2.setName(null);
        c2.setStore(s);
        r.save(c2);
    }

    @Test(expected = javax.validation.ConstraintViolationException.class)
    public void testSetEmptyName() {
        Category c2 = new Category();
        c2.setName("");
        c2.setStore(s);
        r.save(c2);
    }

    @Test
    public void testParent() {
        Category c2 = new Category();
        c2.setName("Child");
        c2.setParent(c);
        c2.setStore(s);
        r.save(c2);
        Category c3 = r.findOne(c2.getId());
        assertEquals(c, c3.getParent());
    }

    @Test
    public void testDelete() {
        r.delete(c);
        Category c2 = r.findOne(c.getId());
        assertNull(c2);
    }
}
