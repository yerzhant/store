import { Observable } from 'rxjs/Observable';

import { BaseHttpService } from './base.http.service';

import { Role } from '../domain/role';

export class RoleService extends BaseHttpService {
    private api = this.server + '/api/security/rolesxxx';

    getAll(): Observable<Role[]> {
        return this.http.get(this.api)
            .map(this.extractData)
            .catch(this.handleError);
    }

    add(role: Role): Observable<Role> {
        return this.http.post(this.api, JSON.stringify(role), this.options)
            .map(this.extractData)
            .catch(this.handleError);
    }
}
