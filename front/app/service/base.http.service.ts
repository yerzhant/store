import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';

import { NotificationService } from './notification.service';

import { Observable } from 'rxjs/Observable';

@Injectable()
export class BaseHttpService {
    protected server = 'http://localhost:8080'
    protected options: RequestOptions;

    constructor(protected http: Http, protected notificationService: NotificationService) {
        this.options = new RequestOptions({ headers: new Headers({ 'Content-Type': 'application/json' }) });
    }

    protected extractData(res: Response) {
        return res.json().data; // || {};
    }

    protected handleError(e: any) {
        let msg = e.message ? e.message : e.status ? `${e.status} - ${e.statusText}` : 'Service error';
        console.error(e);
        // this.notificationService.add({severity: 'info', summary: 'Error', detail: msg});
        console.error('rrrrrrrrrrrrr');
        return Observable.throw(msg);
    }
}
