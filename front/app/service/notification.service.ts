import { Subject } from 'rxjs/Subject';

import { Message } from 'primeng/primeng';

export class NotificationService {
    private source = new Subject<Message>();

    stream = this.source.asObservable();

    add(message: Message) {
        this.source.next(message);
    }
}
