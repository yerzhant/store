import { Observable } from 'rxjs/Observable';

import { BaseHttpService } from './base.http.service';

import { User } from '../domain/user';

export class AuthService extends BaseHttpService {
    private api = 'api/authentication';

    login(user: User): Observable<string> {
        return this.http.post(this.api + '/login', user)
            .map(this.extractData)
            .catch(this.handleError);
    }
}
