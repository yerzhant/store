import { Component, OnInit } from '@angular/core';

import { RoleService } from '../../service/role.service';
import { NotificationService } from '../../service/notification.service';

import { Role } from '../../domain/role';

@Component({
    moduleId: module.id,
    selector: 'roles',
    templateUrl: 'roles.component.html',
})
export class RolesComponent implements OnInit {
    roles: Role[];
    error: string;

    constructor(private roleService: RoleService, private notificationService: NotificationService) { }

    ngOnInit() {
        this.roleService.getAll().subscribe(
            roles => this.roles = roles,
            error => this.notificationService.add({summary:'sum', detail: 'dddt'})
        );
    }
}
