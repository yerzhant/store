import { Component } from '@angular/core';

import { Message } from 'primeng/primeng';

import { NotificationService } from '../service/notification.service';

@Component({
    moduleId: module.id,
    selector: 'app',
    templateUrl: 'app.component.html'
})
export class AppComponent {
    message: Message = {};
    showMessage: boolean;

    constructor(private notificationService: NotificationService) {
        this.notificationService.stream.subscribe(
            message => {
                this.message = message;
                this.showMessage = true;
            }
        );
    }
}
