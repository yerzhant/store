import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../service/auth.service';

import { User } from '../domain/user';

@Component({
    moduleId: module.id,
    selector: 'login',
    templateUrl: 'login.component.html',
})
export class LoginComponent {
    user: User;
    error: string;

    constructor(private authService: AuthService, private router: Router) { }

    login() {
        this.authService.login(this.user).subscribe(
            () => this.router.navigate(['/roles']),
            error => this.error = error
        );
    }
}
