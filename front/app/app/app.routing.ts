import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RolesComponent } from './admin/roles.component';

const appRoutes: Routes = [
    // { path: 'crisis-center', component: CrisisCenterComponent },
    { path: 'roles', component: RolesComponent },
    // { path: 'hero/:id', component: HeroDetailComponent },
    // { path: '**', component: PageNotFoundComponent },
    { path: '', redirectTo: '/roles', pathMatch: 'full' }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
