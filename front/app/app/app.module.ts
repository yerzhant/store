import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { DialogModule } from 'primeng/primeng';

import { routing } from './app.routing';
import { AppComponent } from './app.component';
import { RolesComponent } from './admin/roles.component';

import { RoleService } from '../service/role.service';
import { NotificationService } from '../service/notification.service';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        routing,

        DialogModule
    ],
    providers: [
        RoleService,
        NotificationService
    ],
    declarations: [AppComponent, RolesComponent],
    bootstrap: [AppComponent]
})
export class AppModule { }
